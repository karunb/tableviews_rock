//
//  FirstViewController.swift
//  TABLEViews_Rock!
//
//  Created by Bourishetty,Karun on 2/19/19.
//  Copyright © 2019 Bourishetty,Karun. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var towns = ["NewYOrk","London","Amsterdam","Hyderabad","Sydney"]
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return towns.count
        }
        else{
            return -1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "town")!
        cell.textLabel?.text = towns[indexPath.row]
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}


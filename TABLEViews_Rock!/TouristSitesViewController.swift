//

//  SecondViewController.swift

//  touristSites

//

//  Created by Student on 2/20/19.

//  Copyright © 2019 Student. All rights reserved.

//



import UIKit



class TouristSitesViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    let TouristSites: [String] = ["Walt Disney","Magic Kingdom Park","Machu Pichu"]
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return TouristSites.count
        }
        else{
            return -1
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
    
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "touristSites")!
        cell.textLabel?.text = TouristSites[indexPath.row]
        return cell
        
    }
    
    
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        
        
        
        
        
    }
    
    
    
    
    
}
